class WrongFieldsError(Exception):
    def __init__(self, valids, wrongs):
        self.msg = {"error": "Campos inválidos passados na requisição!",
                    "Campos aceitos": valids, "Errados": wrongs}


class MissingFieldError(Exception):
    def __init__(self, valids, missing):
        self.msg = {"error": "Campo obrigatório não informado na requisição!",
                    "Campos obrigatórios": valids, "Faltando": missing}


class FieldTypeError(Exception):
    def __init__(self):
        self.msg = {
            "error": "Os campos 'app' e 'password' devem ser do tipo string!"}


class InvalidTypesError(Exception):
    def __init__(self, valids, invalids):
        types_verbose = {str: "string", int: "integer",
                         list: "lista/array", dict: "dicionário/objeto"}
        self.msg = {"error": "Campos informados com tipos incorretos!", "Tipos corretos": dict([(field, types_verbose[inf]) for field, inf in valids.items(
        )]), "Incorretos recebidos": dict([(field, types_verbose[type(inf)]) for field, inf in invalids])}


class InvalidEmailError(Exception):
    def __init__(self):
        self.msg = {"error": "E-mail inválido!"}


class InvalidPasswordError(Exception):
    def __init__(self):
        self.msg = {"error": "A senha deve conter pelo menos 1 caractere!"}
