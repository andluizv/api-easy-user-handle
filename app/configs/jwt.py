from flask_jwt_extended import JWTManager
from flask import Flask


def init_jwt(app: Flask):
    JWTManager(app)
