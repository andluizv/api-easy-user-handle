from flask import Flask
from flask_migrate import Migrate


def init_app(app: Flask):
    # TABELAS
    from app.models.space_model import SpaceModel
    from app.models.user_model import UserModel
    Migrate(app, app.db)
