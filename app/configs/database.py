from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from environs import Env

db = SQLAlchemy()


def init_app(app: Flask):
    env = Env()
    env.read_env()

    app.config[
        "SQLALCHEMY_DATABASE_URI"] = env("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config['JSON_SORT_KEYS'] = False
    app.config['JWT_SECRET_KEY'] = env('JWT_SECRET_KEY')

    db.init_app(app)
    app.db = db
