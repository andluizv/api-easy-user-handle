from app.models.space_model import SpaceModel
from flask import request, jsonify, current_app
from app.exceptions import WrongFieldsError, MissingFieldError, FieldTypeError, InvalidPasswordError
from sqlalchemy.exc import IntegrityError


def create_space():
    try:
        data = request.json
        SpaceModel.validate_fields(data)
        data['app'] = data['app'].lower()
        new_space = SpaceModel(**data)

        current_app.db.session.add(new_space)
        current_app.db.session.commit()

        return jsonify({"msg": f'Espaço {new_space.app} criado com sucesso!', "url": f'https://easy-user-handle.herokuapp.com/{new_space.app}'}), 201
    except (WrongFieldsError, MissingFieldError, FieldTypeError, InvalidPasswordError) as e:
        return e.msg, 400
    except IntegrityError:
        return {'error': "Esse nome já está em uso!"}, 409


def get_users(app):
    space = SpaceModel.query.filter_by(app=app).first()
    if not space:
        return {"error": "App não encontrado!"}, 404

    return jsonify(space.users), 200


def delete_space(app):
    try:
        password = request.json['password']
        space = SpaceModel.query.filter_by(app=app).first()
        if not space:
            return {'error': "Esse App não existe ou já foi deletado!"}, 404

        if space.verify_password(password):
            current_app.db.session.delete(space)
            current_app.db.session.commit()
        else:
            return {"error": "Senha incorreta!"}, 401

        return "", 204
    except KeyError:
        return {"error": "Para deletar um App é necessário passar o 'password' usado na criação!"}, 400
