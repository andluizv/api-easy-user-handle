from dataclasses import asdict
from datetime import timedelta
from flask import request, current_app, jsonify
from app.models.user_model import UserModel
from app.models.space_model import SpaceModel
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from app.exceptions import InvalidTypesError, MissingFieldError, WrongFieldsError, InvalidEmailError, InvalidPasswordError


def search_infos(app, email):
    space = SpaceModel.query.filter_by(app=app).first()
    user = UserModel.query.filter_by(
        email=email, space=space.id).first()
    return space, user


def create_user(app):
    try:
        data = request.json
        space, user_in_db = search_infos(app, data['email'])
        if not space:
            return {'error': 'App incorreto ou inexistente!'}, 404

        if user_in_db:
            return {'error': "E-mail já cadastrado!"}, 409

        UserModel.validate_fields(data, 'create')
        data['space'] = space.id
        new_user = UserModel(**data)

        current_app.db.session.add(new_user)
        current_app.db.session.commit()

        user_dict = asdict(new_user)
        token = create_access_token(new_user, expires_delta=timedelta(days=1))
        user_dict['token'] = token
        return jsonify(user_dict), 201
    except (InvalidTypesError, WrongFieldsError, MissingFieldError, InvalidEmailError, InvalidPasswordError) as e:
        return e.msg, 400


def login(app):
    try:
        data = request.json
        UserModel.validate_fields(data, 'login')
        space, user = search_infos(app, data['email'])
        if not space:
            return {'error': 'App incorreto ou inexistente!'}, 404

        if not user:
            return {"error": "Usuário não encontrado!"}, 404

        if user.verify_password(data['password']):
            token = create_access_token(user, expires_delta=timedelta(days=1))
            infos = asdict(user)
            infos['token'] = token
            return jsonify(infos), 200
        else:
            return {'error': "Senha incorreta"}, 401
    except (InvalidTypesError, WrongFieldsError, MissingFieldError) as e:
        return e.msg, 400


@jwt_required()
def get_infos(app):
    space, user = search_infos(app, get_jwt_identity()[
        'email'])
    if not space:
        return {'error': 'App incorreto ou inexistente!'}, 404
    if not user:
        return {'error': "Usuário não encontrado, refaça o login!"}, 404
    return jsonify(user), 200


@jwt_required()
def update_user(app):
    try:
        space, user = search_infos(app, get_jwt_identity()[
            'email'])
        if not space:
            return {'error': 'App incorreto ou inexistente!'}, 404

        if not user:
            return {"error": "Usuário não encontrado, refaça o login!"}, 404
        data = request.json
        UserModel.validate_fields(data, 'update')

        extras = {}
        if "extras" in data.keys():
            extras = data['extras']
            del data['extras']

        for key, value in data.items():
            setattr(user, key, value)
        user.extras = {**user.extras, **extras}

        current_app.db.session.add(user)
        current_app.db.session.commit()

        if "email" in data.keys():
            infos = asdict(user)
            token = create_access_token(user, expires_delta=timedelta(days=1))
            infos['token'] = token
            return jsonify(infos), 200
        return jsonify(user), 200
    except (InvalidTypesError, WrongFieldsError, MissingFieldError) as e:
        return e.msg, 400


@jwt_required()
def delete_user(app):
    space, user = search_infos(app, get_jwt_identity()[
        'email'])

    if not space:
        return {'error': 'App incorreto ou inexistente!'}, 404
    if not user:
        return {"error": "Usuário desconhecido, provavelmente já deletado!"}, 404

    email = user.email
    current_app.db.session.delete(user)
    current_app.db.session.commit()

    return {"msg": f"Usuário {email} deletado!"}
