from app.configs.database import db
from dataclasses import dataclass
from werkzeug.security import generate_password_hash, check_password_hash
from app.exceptions import WrongFieldsError, MissingFieldError, FieldTypeError, InvalidPasswordError


@dataclass
class SpaceModel(db.Model):
    id: int
    app: str

    __tablename__ = 'spaces'

    id = db.Column(db.Integer, primary_key=True)
    app = db.Column(db.String, nullable=False, unique=True)
    password_hash = db.Column(db.String(511), nullable=False)

    users = db.relationship("UserModel", cascade="all, delete")

    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)

    @staticmethod
    def validate_fields(fields):
        valid_fields = ["app", "password"]
        missing_fields = []
        wrong_fields = []

        for field in valid_fields:
            if field not in fields:
                missing_fields.append(field)
        if missing_fields:
            raise MissingFieldError(valid_fields, missing_fields)
        if fields['password'] == "":
            raise InvalidPasswordError

        if type(fields['app']) != str:
            raise FieldTypeError
        if type(fields['password']) != str:
            raise FieldTypeError

        for field in fields:
            if field not in valid_fields:
                wrong_fields.append(field)
        if wrong_fields:
            raise WrongFieldsError(valid_fields, wrong_fields)
