from app.configs.database import db
from dataclasses import dataclass
from werkzeug.security import generate_password_hash, check_password_hash
import re
from app.exceptions import MissingFieldError, InvalidTypesError, WrongFieldsError, InvalidEmailError, InvalidPasswordError


@dataclass
class UserModel(db.Model):
    email: str
    extras: dict

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False)
    space = db.Column(db.Integer, db.ForeignKey('spaces.id'), nullable=False)
    password_hash = db.Column(db.String(511), nullable=False)
    extras = db.Column(db.PickleType, default={})

    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)

    @staticmethod
    def validate_fields(fields, route):
        valid_types = {"email": str, "password": str, "extras": dict}
        required_fields = ['email', 'password']
        missing_fields = []
        invalid_types = []
        extra_fields = []

        # Essa ordem é importante! Campos extras na verificação de tipos gera erro!
        def verify_missing():
            for field in required_fields:
                if field not in fields:
                    missing_fields.append(field)
            if missing_fields:
                raise MissingFieldError(required_fields, missing_fields)
            pattern = re.compile(
                r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
            if not re.fullmatch(pattern, fields['email']):
                raise InvalidEmailError
            if fields['password'] == "":
                raise InvalidPasswordError

        def verify_extra():
            for field in fields:
                if field not in valid_types.keys():
                    extra_fields.append(field)
            if extra_fields:
                raise WrongFieldsError(list(valid_types.keys()), extra_fields)

        def verify_types():
            for field in fields:
                if type(fields[field]) != valid_types[field]:
                    invalid_types.append((field, fields[field]))
            if invalid_types:
                raise InvalidTypesError(valid_types, invalid_types)

        # Adaptando as necessidades de cada rota
        if route == 'create':
            verify_missing()
            verify_extra()
            verify_types()

        if route == 'login':
            verify_missing()
            for field in fields:
                if field not in required_fields:
                    extra_fields.append(field)
            if extra_fields:
                raise WrongFieldsError(required_fields, extra_fields)
            verify_types()

        if route == 'update':
            verify_extra()
            if "email" in fields:
                pattern = re.compile(
                    r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
                if not re.fullmatch(pattern, fields['email']):
                    raise InvalidEmailError
            verify_types()
