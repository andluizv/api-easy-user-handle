from flask import Blueprint
from app.controllers import user_controller

bp_user = Blueprint('bp_user', __name__, url_prefix='/<app>')

bp_user.post('')(user_controller.create_user)
bp_user.post('/login')(user_controller.login)
bp_user.get('')(user_controller.get_infos)
bp_user.patch('')(user_controller.update_user)
bp_user.delete('')(user_controller.delete_user)
