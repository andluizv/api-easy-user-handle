from flask import Blueprint
from app.controllers import space_controller

bp_spaces = Blueprint('bp_spaces', __name__, url_prefix='/spaces')

bp_spaces.post('')(space_controller.create_space)
bp_spaces.delete('/<app>')(space_controller.delete_space)
bp_spaces.get('/<app>')(space_controller.get_users)
