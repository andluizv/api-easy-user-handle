from flask import Flask
from app.blueprints.user_blueprint import bp_user
from app.blueprints.space_blueprint import bp_spaces
from app.blueprints.home_blueprint import bp_home


def init_app(app: Flask):
    app.register_blueprint(bp_home)
    app.register_blueprint(bp_spaces)
    app.register_blueprint(bp_user)
