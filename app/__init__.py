from flask import Flask
from flask_cors import CORS
from app.configs.jwt import init_jwt
from app.configs import database, migrations
from app import blueprints


def create_app():
    app = Flask(__name__)

    CORS(app)

    database.init_app(app)
    migrations.init_app(app)
    init_jwt(app)
    blueprints.init_app(app)

    return app
