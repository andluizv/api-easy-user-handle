# Bem vindo ao Easy-User-Handle!

Essa API é para quem está praticando seu front-end e precisa de um local para salvar dados de login de usuários. Nela você cria um espaço para sua aplicação onde é possível criar usuários usando e-mail e senha, e usar um campo 'extra' para guardar qualquer informação sobre o usuário que sua aplicação precise (menos arquivos). Por exemplo, você cria uma aplicação que além de logar usuários (e-mail e senha), você deseja salvar nome, idade e uma url de imagem, dentro da chave 'extras' é possível guardar essas chaves com as informações que precisa pra testar a aplicação!

> No momento essa API está rodando no Heroku... existem algumas limitações por conta disso... a API só pode rodar por um determinado tempo por mês (500 horas) então eventualmente pode estar fora do ar (especialmente final de mês) e se fouma requisição que está "acordando" o Heroku ela pode demorar um pouco mais (aquele 'loading...' fica interessante nesses casos)

## Rotas da API:

*A url base da API é:*

>[https://easy-user-handle.herokuapp.com/](https://easy-user-handle.herokuapp.com/)
### Referentes ao espaço (App)

Um espaço nada mais é do que o nome que será usado na url em que serão realizadas as requisições para criar, logar, atualizar, visualizar ou deletar o usuário.

>Cada espaço é único, logo um mesmo e-mail pode ser cadastrado em 2 (dois) espaços diferentes, mas não pode haver cadastro de e-mails iguais no mesmo espaço.
>
#### - Criação do espaço (App):

###### **POST** /spaces

> Pode ser feito aqui: [https://easy-user-handle-signup-pg.vercel.app/](https://easy-user-handle-signup-pg.vercel.app/)

##### Campos obrigatórios:

 - "app" => string
 - "password" => string (é necessário caso deseje deletar o espaço)

Exemplo de requisição:
```json
{
	"app": "praticandoforms",
	"password": "1234"
}
```
Exemplo de resposta:
```json
{
	"msg": "Espaço praticandoforms criado com sucesso!",
	"url": "https://easy-user-handle.herokuapp.com/praticandoforms"	
}
```
#### - Buscar usuários cadastrados em um espaço:
**GET** /spaces/{app}
> app seria o nome dado ao espaço e usado nas requisições.

Exemplo de resposta:
```json
[
	{
		"email": "zezinho@mail.com",
		"extras": {}
	},
	{
		"email": "luizinha@mail.com",
		"extras": {}
	}
]
```
#### - Deletar espaço:
**DELETE** /spaces/{app}
##### Campos obrigatórios:
- "password": string  (senha usada na criação do espaço)

Exemplo de requisição:
```json
{
	"password": "1234"
}
```
Resposta: **204 NO CONTENT**

### Referentes ao usuário

A ideia é usar um dado padrão para fazer o login, no caso é usado o e-mail, e ter liberdade para adicionar qualquer outra informação a ser usada na aplicação.
#### - Criar novo usuário:
**POST** /{app}

> Lembrando: app é o nome que foi cadastrado na criação do espaço, o caminho completo seria algo assim: https://easy-user-handle.herokuapp.com/praticandoforms, onde "praticandoforms" seria o nome que foi cadastrado na criação do espaço.

##### Campos obrigatórios:
- "email" => string (Ocorre uma validação pela API)
- "password" => string (a API não faz limitação, caso deseje usar tamanho mínimo, ou especificar caracteres a serem usados, deve ser feito no Front!)

Exemplo de requisição:
```json
{
	"email": "usuario@email.com",
	"password": "1234"
}
```
Exemplo de resposta:
```json
{
	"email": "usuario@email.com",
	"extras": {},
	"token": "askjdfgJHsajkhdfkg..."
}
```
Neste caso o extras está vazio, mas pode ser usado já na criação do usuário... Passar nome, endereço, idade por exemplo:
```json
{
	"email": "usuario@email.com",
	"password": "1234",
	"extras": {
		"nome": "Um Usuário",
		"idade": 20,
		"endereco": "Rua exemplar, 101"
	}
}
```
> - Qualquer informação pode ser usada dentro do "extras", desde que seja do tipo chave-valor. 
> - O token tem validade de 24 horas. Se sua aplicação for redirecionar caso o usuário já esteja logado (local storage, session storage, cookie), é uma ideia verificar o tempo de validade do token pra evitar surpresas!

Se tentar passar o extra como outro objeto que não seja um de chave-valor(dicionário) irá gerar um erro:
```json
{
  "error": "Campos informados com tipos incorretos!",
  "Tipos corretos": {
    "email": "string",
    "password": "string",
    "extras": "dicionário/objeto"
  },
  "Incorretos recebidos": {
    "extras": "lista/array"
  }
}
```

#### - Logar usuário:
POST /{app}/login

##### Campos obrigatórios:
- "email" => string
- "password" => string

Exemplo de requisição:

```json
{
	"email": "usuario@email.com",
	"password": "1234"
}
```

Exemplo de resposta:
```json
{
	"email": "usuario@email.com",
	"extras": {},
	"token": "askjdfgJHsajkhdfkg..."
}
```

#### - Dados do usuário:
GET /{app}

> Esta rota necessita que o token seja passado do header da requisição no tipo Bearer. Sugestão: https://stackoverflow.com/questions/40988238/sending-the-bearer-token-with-axios

Exemplo de resposta:
```json
{
	"email": "usuario@email.com",
	"extras": {
		"nome": "Um Usuário",
		"idade": 20,
		"endereco": "Rua exemplar, 101",
		"urlAvatar": "http://imagem.com.br"
	}
}
```

#### - Atualizar usuário:
Patch /{app}

> Esta rota necessita que o token seja passado do header da requisição no tipo Bearer. Sugestão: https://stackoverflow.com/questions/40988238/sending-the-bearer-token-with-axios

##### Campos:

- Todos os dados podem ser alterados (email, senha ou extras). Novas chaves podem ser adicionadas ao "extras", porém nenhuma poderá ser excluída. Se o e-mail for alterado um novo token será gerado na resposta para atualizar a aplicação e evitar erros inesperados.

#### - Deletar usuário:
Delete /{app}

> Esta rota necessita que o token seja passado do header da requisição no tipo Bearer. Sugestão: https://stackoverflow.com/questions/40988238/sending-the-bearer-token-with-axios

##### Campos obrigatórios:
- "password" => string
